package com.liferay.samplestrutsaction.hook.action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.gcube.portal.tou.removeaccount.thread.RemovedUserAccountThread;

import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;
import com.liferay.portal.model.User;
import com.liferay.portal.util.PortalUtil;

/**
 * @author massi
 */
public class UpdateTermsOfUseAction extends BaseStrutsAction {
	public String execute(StrutsAction originalStrutsAction, HttpServletRequest request, HttpServletResponse response)	throws Exception {
		String agreement = request.getParameter("agreement");
		if (agreement.compareTo("i-disagree")==0) { //the user did not agree
			String username2Delete = null;
			try {
				User user = PortalUtil.getUser(request);
				username2Delete = user.getScreenName();				
			} catch (Exception e) {			
				e.printStackTrace();
			}
			System.out.println("*** UpdateTermsOfUseAction user DID NOT AGREE, removeUser account for username=" + username2Delete + " ongoing ... ");
			if (username2Delete != null) {
				Thread dropUserAccountThread = new Thread(new RemovedUserAccountThread(username2Delete));
				dropUserAccountThread.start();
			} else  {
				System.out.println("Account not removed");
			}
			
			return "/../c/portal/logout";
		}
		else //the user agreed
			return originalStrutsAction.execute(request, response);
	} 
}
